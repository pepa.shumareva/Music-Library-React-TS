import React from 'react'

function GuestNav() {
 return (
  <div className="guest">
   <a href="/login" className='text-[1.5rem] color-[#0a0a0a] font-bold mr-2'>Login</a>
   <a href="/register" className='text-[1.5rem] color-[#0a0a0a] font-bold'>Register</a>
  </div>
 )
}

export default GuestNav
