import React from 'react'

function Footer() {
 return (
  <footer className='bg-[#cbf6ff] text-black fw-extrabold text-center p-[10px] fixed bottom-0 left-0 w-[100%] h-[30px] shadow-[0_-5px_5px_#cbf6ff]'>
   <p>@MusicLibrary</p>
  </footer>
 )
}

export default Footer
